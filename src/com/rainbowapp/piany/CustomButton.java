package com.rainbowapp.piany;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;


public class CustomButton extends View {
	static int prevSoundId;
	//ActivityからContext(この場合描写先、ということ？)だけを指定してインスタンスを作成したときに呼び出される
	//コンストラクタ
	public CustomButton(Context cont) {
		super(cont);
	}

	//xmlファイルにuri+クラス名でタグを作って、アクティビティに追加するときに呼び出されるコンストラクタ
	//AttributeSetというのはタグ内にあるlayout_width=""などらしい
	public CustomButton(Context cont, AttributeSet attr) {
		super(cont,attr);
	}
	
	@Override
	protected void onDraw(Canvas canvas){
		canvas.drawColor(Color.BLACK);
	}
	
	//xmlでlayout_width,layout_heightを指定すると指定したサイズに応じた大きさになります
	//(ただしspでなくて画素とかのような気がする……？)
	//コンストラクタだけでActivityから呼び出した(=AttributeSetなしで呼び出した)ときは、widthMeasureSpecなどは画面の縦幅、横幅になるみたいです。
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec){
		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
	    int widthSize = MeasureSpec.getSize(widthMeasureSpec);
	    int heightMode = MeasureSpec.getMode(heightMeasureSpec);
	    int heightSize = MeasureSpec.getSize(heightMeasureSpec);
		setMeasuredDimension(widthSize,heightSize);
	}
	
	public boolean onTouchEvent(MotionEvent event){
		
		switch (event.getAction()) {
	    case MotionEvent.ACTION_DOWN:
	    	//音を鳴らす
	    	Log.d("ACTION_DOWN","音が鳴る！");
	        //Log.d("TouchEvent", "getAction()" + "ACTION_DOWN" + this.getId());
	        prevSoundId = this.getId();
	        break;
	    case MotionEvent.ACTION_UP:
	        Log.d("TouchEvent", "getAction()" + "ACTION_UP");
	        break;
	    case MotionEvent.ACTION_MOVE:
	    	if(prevSoundId != this.getId()){
	    		Log.d("ACTION_MOVE","音が鳴る！");
	    	}
	    	prevSoundId = this.getId();
	        //Log.d("TouchEvent", "getAction()" + "ACTION_MOVE");
	        break;
	    case MotionEvent.ACTION_CANCEL:
	        //Log.d("TouchEvent", "getAction()" + "ACTION_CANCEL");
	        break;
	    }
		return true;
	}
	
}