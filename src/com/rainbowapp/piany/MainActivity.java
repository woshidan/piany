package com.rainbowapp.piany;

import java.util.ArrayList;

import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.view.KeyEvent;
import android.util.Log;

public class MainActivity extends Activity {
	
	//楽器の選択肢
	Button select_instrument;
	
	/*array.add("ドラム");
	array.add("ピアノ１");
	array.add("ピアノ２");*/

	//音声素材の取得用のフィールドの宣言
	//ドラム
	private instrument drum = new instrument();
	private instrument piano1 = new instrument();
	private instrument piano2 = new instrument();
	private SoundPool soundPool;
	
	//現在の楽器
	private instrument playing_instrument = new instrument();
	
	//鍵盤のボタン
	private Button key_c1;
	private Button key_c1s;
	private Button key_d1;
	private Button key_d1s;
	private Button key_e1;
	private Button key_f1;
	private Button key_f1s;
	private Button key_g1;
	private Button key_g1s;
	private Button key_a2;
	private Button key_a2s;
	private Button key_b2;
	private Button key_c2;
	
	private String key_name[] = {"key_c1",
			"key_c1s","key_d1","key_d1s","key_e1",
			"key_f1","key_f1s","key_g1","key_g1s",
			"key_a2","key_a2s","key_b2","key_c2"};
	private Button keys[]={key_c1,
			key_c1s,key_d1,key_d1s,key_e1,
			key_f1,key_f1s,key_g1,key_g1s,
			key_a2,key_a2s,key_b2,key_c2};
	
	//音声素材の読み込み。全部一括で行う
	void loadInstrumentsId(){
		soundPool = new SoundPool(2,AudioManager.STREAM_MUSIC,0);
		
		Context applicationContext = getApplicationContext();
		
		//ドラム
		drum.C1 = soundPool.load(applicationContext, R.raw.drumc1,1);
		drum.C1s = soundPool.load(applicationContext, R.raw.drumcs1,1);
		drum.D1 = soundPool.load(applicationContext, R.raw.drumd1,1);
		drum.D1s = soundPool.load(applicationContext, R.raw.drumds1,1);
		drum.E1 = soundPool.load(applicationContext, R.raw.drume1,1);
		drum.F1 = soundPool.load(applicationContext, R.raw.drumf1,1);
		drum.F1s = soundPool.load(applicationContext, R.raw.drumfs1,1);
		drum.G1 = soundPool.load(applicationContext, R.raw.drumg1,1);
		drum.G1s = soundPool.load(applicationContext, R.raw.drumgs1,1);
		drum.A2 = soundPool.load(applicationContext, R.raw.druma2,1);
		drum.A2s = soundPool.load(applicationContext, R.raw.drumas2,1);
		drum.B2 = soundPool.load(applicationContext, R.raw.drumb2,1);
		drum.C2 = soundPool.load(applicationContext, R.raw.drumc2,1);

		//ピアノ1
		piano1.C1 = soundPool.load(applicationContext, R.raw.pianoc1,1);
		piano1.C1s = soundPool.load(applicationContext, R.raw.pianocs1,1);
		piano1.D1 = soundPool.load(applicationContext, R.raw.pianod1,1);
		piano1.D1s = soundPool.load(applicationContext, R.raw.pianods1,1);
		piano1.E1 = soundPool.load(applicationContext, R.raw.pianoe1,1);
		piano1.F1 = soundPool.load(applicationContext, R.raw.pianof1,1);
		piano1.F1s = soundPool.load(applicationContext, R.raw.pianofs1,1);
		piano1.G1 = soundPool.load(applicationContext, R.raw.pianog1,1);
		piano1.G1s = soundPool.load(applicationContext, R.raw.pianogs1,1);
		piano1.A2 = soundPool.load(applicationContext, R.raw.pianoa2,1);
		piano1.A2s = soundPool.load(applicationContext, R.raw.pianoas2,1);
		piano1.B2 = soundPool.load(applicationContext, R.raw.pianob2,1);
		piano1.C2 = soundPool.load(applicationContext, R.raw.pianoc2,1);
	
		//ピアノ2
		piano2.C1 = soundPool.load(applicationContext, R.raw.pianoc2,1);
		piano2.C1s = soundPool.load(applicationContext, R.raw.pianocs2,1);
		piano2.D1 = soundPool.load(applicationContext, R.raw.pianod2,1);
		piano2.D1s = soundPool.load(applicationContext, R.raw.pianods2,1);
		piano2.E1 = soundPool.load(applicationContext, R.raw.pianoe2,1);
		piano2.F1 = soundPool.load(applicationContext, R.raw.pianof2,1);
		piano2.F1s = soundPool.load(applicationContext, R.raw.pianofs2,1);
		piano2.G1 = soundPool.load(applicationContext, R.raw.pianog2,1);
		piano2.G1s = soundPool.load(applicationContext, R.raw.pianogs2,1);
		piano2.A2 = soundPool.load(applicationContext, R.raw.pianoa3,1);
		piano2.A2s = soundPool.load(applicationContext, R.raw.pianoas3,1);
		piano2.B2 = soundPool.load(applicationContext, R.raw.pianob3,1);
		piano2.C2 = soundPool.load(applicationContext, R.raw.pianoc3,1);
		
	}
	
	@Override
	protected void onDestroy(){
		if(soundPool != null){
			soundPool.unload(drum.C1);
			soundPool.unload(drum.C1s);
			soundPool.unload(drum.D1);
			soundPool.unload(drum.D1s);
			soundPool.unload(drum.E1);
			soundPool.unload(drum.F1);
			soundPool.unload(drum.F1s);
			soundPool.unload(drum.G1);
			soundPool.unload(drum.G1s);
			soundPool.unload(drum.A2);
			soundPool.unload(drum.A2s);
			soundPool.unload(drum.B2);
			soundPool.unload(drum.C2);
			
			soundPool.unload(piano1.C1);
			soundPool.unload(piano1.C1s);
			soundPool.unload(piano1.D1);
			soundPool.unload(piano1.D1s);
			soundPool.unload(piano1.E1);
			soundPool.unload(piano1.F1);
			soundPool.unload(piano1.F1s);
			soundPool.unload(piano1.G1);
			soundPool.unload(piano1.G1s);
			soundPool.unload(piano1.A2);
			soundPool.unload(piano1.A2s);
			soundPool.unload(piano1.B2);
			soundPool.unload(piano1.C2);
			
			soundPool.unload(piano2.C1);
			soundPool.unload(piano2.C1s);
			soundPool.unload(piano2.D1);
			soundPool.unload(piano2.D1s);
			soundPool.unload(piano2.E1);
			soundPool.unload(piano2.F1);
			soundPool.unload(piano2.F1s);
			soundPool.unload(piano2.G1);
			soundPool.unload(piano2.G1s);
			soundPool.unload(piano2.A2);
			soundPool.unload(piano2.A2s);
			soundPool.unload(piano2.B2);
			soundPool.unload(piano2.C2);
			
			soundPool.release();
			soundPool = null;
		}
		super.onDestroy();
	}
	
	private void setupKeys(){
		int ButtonId;
		for(int i=0;i<13;i++){
			ButtonId = getResources().getIdentifier(key_name[i], "id", getPackageName());
			keys[i] = (Button) findViewById(ButtonId);
		}
	}
	
	
	
	private void setupKeysSound(instrument instrument){
		
	if(instrument == drum){
			keys[0].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(drum.C1, 1.0f, 1.0f, 1, 0, 1);
				}
				
			});
			keys[1].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(drum.C1s, 1.0f, 1.0f, 2, 0, 1);
				}
			
			});
			keys[2].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(drum.D1, 1.0f, 1.0f, 3, 0, 1);
				}
			
			});
			keys[3].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(drum.D1s, 1.0f, 1.0f, 4, 0, 1);
				}
			
			});
			keys[4].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(drum.E1, 1.0f, 1.0f, 5, 0, 1);
				}
			
			});
			keys[5].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(drum.F1, 1.0f, 1.0f, 6, 0, 1);
				}
			
			});
			keys[6].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(drum.F1s, 1.0f, 1.0f, 7, 0, 1);
				}
			
			});
			keys[7].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(drum.G1, 1.0f, 1.0f, 8, 0, 1);
				}
			
			});
			keys[8].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(drum.G1s, 1.0f, 1.0f, 9, 0, 1);
				}
			
			});
			keys[9].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(drum.A2, 1.0f, 1.0f, 10, 0, 1);
				}
			
			});
			keys[10].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(drum.A2s, 1.0f, 1.0f, 11, 0, 1);
				}
			
			});
			keys[11].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(drum.B2, 1.0f, 1.0f, 12, 0, 1);
				}
			
			});
			keys[12].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(drum.C2, 1.0f, 1.0f, 13, 0, 1);
				}
			
			});
		}
		if(instrument == piano1){
			keys[0].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(piano1.C1, 1.0f, 1.0f, 1, 0, 1);
				}
				
			});
			keys[1].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(piano1.C1s, 1.0f, 1.0f, 2, 0, 1);
				}
			
			});
			keys[2].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(piano1.D1, 1.0f, 1.0f, 3, 0, 1);
				}
			
			});
			keys[3].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(piano1.D1s, 1.0f, 1.0f, 4, 0, 1);
				}
			
			});
			keys[4].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(piano1.E1, 1.0f, 1.0f, 5, 0, 1);
				}
			
			});
			keys[5].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(piano1.F1, 1.0f, 1.0f, 6, 0, 1);
				}
			
			});
			keys[6].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(piano1.F1s, 1.0f, 1.0f, 7, 0, 1);
				}
			
			});
			keys[7].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(piano1.G1, 1.0f, 1.0f, 8, 0, 1);
				}
			
			});
			keys[8].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(piano1.G1s, 1.0f, 1.0f, 9, 0, 1);
				}
			
			});
			keys[9].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(piano1.A2, 1.0f, 1.0f, 10, 0, 1);
				}
			
			});
			keys[10].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(piano1.A2s, 1.0f, 1.0f, 11, 0, 1);
				}
			
			});
			keys[11].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(piano1.B2, 1.0f, 1.0f, 12, 0, 1);
				}
			
			});
			keys[12].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(piano1.C2, 1.0f, 1.0f, 13, 0, 1);
				}
			
			});
		}
		
		if(instrument == piano2){
			keys[0].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(piano2.C1, 1.0f, 1.0f, 1, 0, 1);
				}
				
			});
			keys[1].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(piano2.C1s, 1.0f, 1.0f, 2, 0, 1);
				}
			
			});
			keys[2].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(piano2.D1, 1.0f, 1.0f, 3, 0, 1);
				}
			
			});
			keys[3].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(piano2.D1s, 1.0f, 1.0f, 4, 0, 1);
				}
			
			});
			keys[4].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(piano2.E1, 1.0f, 1.0f, 5, 0, 1);
				}
			
			});
			keys[5].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(piano2.F1, 1.0f, 1.0f, 6, 0, 1);
				}
			
			});
			keys[6].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(piano2.F1s, 1.0f, 1.0f, 7, 0, 1);
				}
			
			});
			keys[7].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(piano2.G1, 1.0f, 1.0f, 8, 0, 1);
				}
			
			});
			keys[8].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(piano2.G1s, 1.0f, 1.0f, 9, 0, 1);
				}
			
			});
			keys[9].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(piano2.A2, 1.0f, 1.0f, 10, 0, 1);
				}
			
			});
			keys[10].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(piano2.A2s, 1.0f, 1.0f, 11, 0, 1);
				}
			
			});
			keys[11].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(piano2.B2, 1.0f, 1.0f, 12, 0, 1);
				}
			
			});
			keys[12].setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					soundPool.play(piano2.C2, 1.0f, 1.0f, 13, 0, 1);
				}
			
			});
		}
		
	}
	
	private void init(){
		
		//音声素材の読み込み
		loadInstrumentsId();
		System.out.print("After loadInstrument");
		
		//鍵盤ボタンビューの取得
		setupKeys();
		System.out.print("After setupKeys");
		/*key_c1 = (Button) findViewById(R.id.key_c1);
		key_c1s = (Button) findViewById(R.id.key_c1s);
		key_d1 = (Button) findViewById(R.id.key_d1);
		key_d1s = (Button) findViewById(R.id.key_d1s);
		key_e1 = (Button) findViewById(R.id.key_e1);
		key_f1 = (Button) findViewById(R.id.key_f1);
		key_f1s = (Button) findViewById(R.id.key_f1s);
		key_g1 = (Button) findViewById(R.id.key_g1);
		key_g1s = (Button) findViewById(R.id.key_g1s);
		key_a2 = (Button) findViewById(R.id.key_a2);
		key_a2s = (Button) findViewById(R.id.key_a2s);
		key_b2 = (Button) findViewById(R.id.key_b2);
		key_c2 = (Button) findViewById(R.id.key_c2);*/
		
		//楽器の設定
		playing_instrument = piano2;
		//楽器の音をセット
		setupKeysSound(piano2);
		System.out.print("After setUpKeysSound");
		
		select_instrument = (Button) findViewById(R.id.select_instrument);
		select_instrument.setText("ピアノ2"); 
		
		
	}
		
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        System.out.print("Before init");
        init();
        
        select_instrument.setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v){
				if(playing_instrument == piano2){
					playing_instrument = drum;
					setupKeysSound(playing_instrument);
					select_instrument.setText("ドラム");
				}else if(playing_instrument == drum){
					playing_instrument = piano1;
					setupKeysSound(playing_instrument);
					select_instrument.setText("ピアノ1");
				}else{
					playing_instrument = piano2;
					setupKeysSound(playing_instrument);
					select_instrument.setText("ピアノ2");
				}
				
			}
			
		});
        
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
}